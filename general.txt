<?xml version="1.0" encoding="UTF-8"?>
<Duty>
	<StartTime>12:00:00</StartTime>
	<FinishTime>23:55:00</FinishTime>
	<RoundPeriod>00:02:00</RoundPeriod>
	<RoundDelay>00:00:30</RoundDelay>
	<MinPresenceControlInterval>00:00:30</MinPresenceControlInterval>
	<MaxPresenceControlInterval>00:02:00</MaxPresenceControlInterval>
	<PresenceControlReactionTime>00:00:20</PresenceControlReactionTime>
	<NFCPoints>
		<Point>
			<UID>040F2012A84A81</UID>
			<Name>Point 1</Name>
			<DelayBeforeActivation>00:00:30</DelayBeforeActivation>
			<ReadTime>00:00:40</ReadTime>
		</Point>
		<Point>
			<UID>04172012A84A81</UID>
			<Name>Point 2</Name>
			<DelayBeforeActivation>00:01:00</DelayBeforeActivation>
			<ReadTime>00:02:30</ReadTime>
		</Point>
		<Point>
			<UID>04172012A84A81</UID>
			<Name>Point 3</Name>
			<DelayBeforeActivation>00:00:20</DelayBeforeActivation>
			<ReadTime>00:00:50</ReadTime>
		</Point>
	</NFCPoints>
</Duty>